const data = require('./data.json');
const fs = require('fs');
const {detailsOfEmployees, groupEmployeesOnCompany, detailsOfACompany, removeId, sortEmployees, swapCompanies, addBirthdays} = require('./employeeCallbackAsyncAwaitSolution.cjs');
let employeeData = data.employees;
let outputPath = 'output/'
let dataIds = [2,13,23];

async function employeeCallbacksAsyncAwait(employeeData){
    try{
        await detailsOfEmployees(employeeData,dataIds);
        await groupEmployeesOnCompany(employeeData);
        await detailsOfACompany(employeeData);
        await removeId(employeeData);
        await sortEmployees(employeeData);
        await swapCompanies(employeeData);
        let result = await addBirthdays(employeeData);
        console.log(result);
    }catch(err){
        console.error(err.message);
    }
}

employeeCallbacksAsyncAwait(employeeData);