let data = require('./data.json');
let fs = require('fs');

function detailsOfEmployees(employeeData,dataIds){
    return new Promise((resolve,reject)=>{
    const selectedEmployees = employeeData.filter(employee => dataIds.includes(employee.id))
    fs.writeFile('./output/selectedEmployees.json',JSON.stringify(selectedEmployees), err =>{
        if(err) reject(err);
        else resolve(employeeData);    
    })
    }) 
}

function addBirthdays(employeeData){
    return new Promise((resolve,reject)=>{
        let employeeBirthdays = employeeData.map(employee=>{
            if(employee.id%2 === 0) return {...employee, birthday: new Date().toDateString().substring(4)}
            else return employee; 
        })
        fs.writeFile('./output/employeeBirthdays.json',JSON.stringify(employeeBirthdays),err=>{
            if(err) reject(err)
            else resolve('All promises are fulfilled successfully');
        })
    })
}

function swapCompanies(employeeData){
    return new Promise((resolve,reject)=>{
        let swapedData = employeeData;
    let emp92Index = swapedData.findIndex(employee => employee.id===92);
    let emp93Index = swapedData.findIndex(employee => employee.id===93);
    let temp = swapedData[emp92Index].company;
    swapedData[emp92Index].company = swapedData[emp93Index].company;
    swapedData[emp93Index].company = temp;
    fs.writeFile('./output/swapedData.json',JSON.stringify(swapedData),err=>{
         if(err) reject(err)
         else resolve(employeeData);
    })
    })
}

function sortEmployees(employeeData){
    return new Promise((resolve,reject)=>{
        employeeData.sort((a,b)=>{
            if(a.company > b.company) return 1;
            else if(a.company < b.company) return -1;
            else if(a.id > b.id) return 1;
            else if(a.id < b.id) return -1;
            else return 0;
            })
            fs.writeFile('./output/sortedEmployees.json',JSON.stringify(employeeData),err =>{
                if(err) reject(err);
                else resolve(employeeData);
            })
    })
}

function removeId(employeeData){
    return new Promise((resolve,reject)=>{
        let id2Removed = employeeData.filter(employee => employee.id !== 2)
        fs.writeFile('./output/id2Removed.json',JSON.stringify(id2Removed), err =>{
            if(err) reject(err);
            else resolve(employeeData);
        })
    })
        
}


function detailsOfACompany(employeeData){
    return new Promise((resolve,reject)=>{
        let powerpuffBrigadeCompany = employeeData.filter(employee => employee.company==='Powerpuff Brigade');
        fs.writeFile('./output/powerpuffBrigadeCompany.json',JSON.stringify(powerpuffBrigadeCompany),err =>{
            if(err) reject(err);
            else resolve(employeeData)
        })
    })
}

function groupEmployeesOnCompany(employeeData){
    return new Promise((resolve,reject)=>{
        let companyGroup = { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []};
        employeeData.map(employee =>  companyGroup[employee.company].push(employee))
        fs.writeFile('./output/companyGroup.json',JSON.stringify(companyGroup),err =>{
        if(err) reject(err);
        else resolve(employeeData);
    })
    })
}

module.exports = {detailsOfEmployees, groupEmployeesOnCompany, detailsOfACompany, removeId, sortEmployees, swapCompanies, addBirthdays}